cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-streaming-media.StreamingMedia",
      "file": "plugins/cordova-plugin-streaming-media/www/StreamingMedia.js",
      "pluginId": "cordova-plugin-streaming-media",
      "clobbers": [
        "streamingMedia"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-splashscreen": "6.0.0",
    "cordova-plugin-streaming-media": "2.3.0",
    "cordova-plugin-whitelist": "1.3.5"
  };
});