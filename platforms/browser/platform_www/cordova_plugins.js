cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-streaming-media/www/StreamingMedia.js",
        "id": "cordova-plugin-streaming-media.StreamingMedia",
        "pluginId": "cordova-plugin-streaming-media",
        "clobbers": [
            "streamingMedia"
        ]
    },
    {
        "file": "plugins/cordova-plugin-splashscreen/src/browser/SplashScreenProxy.js",
        "id": "cordova-plugin-splashscreen.SplashScreenProxy",
        "pluginId": "cordova-plugin-splashscreen",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
        "id": "cordova-plugin-splashscreen.SplashScreen",
        "pluginId": "cordova-plugin-splashscreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-streaming-media": "2.3.0",
    "cordova-plugin-whitelist": "1.3.5",
    "cordova-plugin-splashscreen": "6.0.0"
}
// BOTTOM OF METADATA
});