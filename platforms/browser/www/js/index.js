$(document).on('click', '.channel-button', function () {
    var _this = $(this);
    window.location.href = "page.html?channel=" + _this.data('channel') + '&img=' + _this.data('img') + '&name=' + _this.data('name');
});

$(function () {
    var channel = GetURLParameter('channel');
    var img = GetURLParameter('img');
    var name = GetURLParameter('name');
    var getStreamURL = getChannel(channel);
    $('.channel-name').html(decodeURIComponent(name));
    $("#selected-channel").attr("src", img);
    $('#stream-play').attr('data-stream', getStreamURL);
});

$(document).on('click', '#stream-play', function () {
    var _this = $(this);
    player(_this.data('stream'));
});

function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

function getChannel(channel) {
    var streamURL = '';
    switch (channel) {
        case 'lifestream_networks':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/lstv/live/playlist.m3u8';
            break;
        case 'prayer_television_network':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/prayernetwork/live/playlist.m3u8';
            break;
        case 'marriage_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/marriagenetwork/live/playlist.m3u8';
            break;
        case 'end_time_prophcy_network':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/endoftime/live/playlist.m3u8';
            break;
        case 'end_time_2_prophcy_network':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/endtimeprophecy2/live/playlist.m3u8';
            break;
        case 'womens_channel_1':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/womentvnetwork1/live/playlist.m3u8';
            break;
        case 'womens_channel_2':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/womentvnetwork2/live/playlist.m3u8';
            break;
        case 'womens_channel_3':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/womentvnetwork3/live/playlist.m3u8';
            break;
        case 'womens_channel_4':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/womentvnetwork4/live/playlist.m3u8';
            break;
        case 'womens_channel_5':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/prayernetwork/live/playlist.m3u8';
            break;
        case 'hell_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/hellchannel/live/playlist.m3u8';
            break;
        case 'church_network':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/churchnetwork/live/playlist.m3u8';
            break;
        case 'moral_freedom':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/moralfreedom/live/playlist.m3u8';
            break;
        case 'drug_free_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/drugfreechannel/live/playlist.m3u8';
            break;
        case 'grief_recovery_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/griefrecovery/live/playlist.m3u8';
            break;
        case 'suicide_alternative_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/suicidealternative/live/playlist.m3u8';
            break;
        case 'forgiveness_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/forgiveness/live/playlist.m3u8';
            break;
        case 'salvation_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/salvationchannel/live/playlist.m3u8';
            break;
        case 'god_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/godchannel/live/playlist.m3u8';
            break;
        case 'mens_channel':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/menschannel/live/playlist.m3u8';
            break;
        case 'lifetream_television_espanol':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/lifestreamespanol/live/playlist.m3u8';
            break;
        case 'lifestream_india':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/lifestreamindia/live/playlist.m3u8';
            break;
        case 'lifestream_brazil':
            streamURL = 'https://5a22f3bfa1a54.streamlock.net/lifestreambrazil/live/playlist.m3u8';
            break;
        default:
            break;
    }

    return streamURL;
}

function player(streamURL) {
    var options = {
        successCallback: function() {
            window.location = "index.html";
        },
        errorCallback: function(errMsg) {
            alert("Error! " + errMsg);
        },
        
        orientation: 'landscape'
    };

    window.plugins.streamingMedia.playVideo(streamURL, options);
}